<?php

namespace YOOtheme\Builder\Wordpress\Source\Type;

use YOOtheme\Builder\Source;

class ChoiceFieldType
{
    /**
     * @param Source $source
     *
     * @return array
     */
    public function __invoke(Source $source)
    {
        return [
            'fields' => [
                'label' => [
                    'type' => 'String',
                    'metadata' => [
                        'label' => 'Label',
                    ],
                ],
                'value' => [
                    'type' => 'String',
                    'metadata' => [
                        'label' => 'Value',
                    ],
                ],
            ],
        ];
    }
}
