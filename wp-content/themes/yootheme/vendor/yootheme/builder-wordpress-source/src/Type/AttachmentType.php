<?php

namespace YOOtheme\Builder\Wordpress\Source\Type;

use YOOtheme\Builder\Source;
use YOOtheme\Str;
use YOOtheme\Url;

class AttachmentType
{
    /**
     * @param Source $source
     *
     * @return array
     */
    public function __invoke(Source $source)
    {
//        $type = get_post_type_object('attachment');
        $props = [
            //            'title',
            //            'name',
            //            'filename',
            'url',
            //            'link',
            'alt',
            //            'description',
            //            'caption',
        ];

        $fields = [];
        foreach ($props as $prop) {
            $fields[$prop] = [
                'type' => 'String',
                'metadata' => [
                    'label' => Str::titleCase($prop),
                ],
            ];
        }

        $resolvers = $source->mapResolvers($this);

        return compact('fields', 'resolvers');
    }

    public function alt($attachmentId)
    {
        return get_post_meta($attachmentId, '_wp_attachment_image_alt', true);
    }

    public function url($attachmentId)
    {
        $url = URL::to(get_attached_file($attachmentId));

        if (Str::startsWith($url, URL::base())) {
            $url = ltrim(substr($url, strlen(URL::base())), '/');
        }

        return $url;
    }
}
