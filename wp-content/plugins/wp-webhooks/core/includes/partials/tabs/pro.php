<div class="wpwh-container">

	<div class="wpwh-box wpwh-box--big mb-3">
		<div class="wpwh-box__body">
			<h2 class="mb-3"><strong>Go Pro</strong> and unlock massive benefits!</h2>
			<ul class="wpwh-checklist wpwh-checklist--two-col">
				<li><strong>Free</strong> access to all of our <a href="https://ironikus.com/products/?fwp_download_categories=wpwh-pro-extension" target="_blank" rel="noopener noreferrer" class="text-secondary"><strong>pro extensions</strong></a>.</li>
				<li>Create users and posts with custom post meta (Advanced custom fields supported)</li>
				<li>Enhanced security through IP whitelists, security tokens and permission limitations</li>
				<li>Logs to keep track of all incoming and outgoing webhook calls</li>
				<li>Our powerful Data Mapping Engine to directly integrate services with your WordPress website</li>
				<li>Personal Assistant Bot within the plugin</li>
				<li>Bulk webhook action: Fire multiple webhook actions within a single call</li>
				<li>Whitelabel feature (For unlimited license holders)</li>
			</ul>
			<a href="https://ironikus.com/compare-wp-webhooks-pro/" target="_blank" class="wpwh-btn wpwh-btn--secondary" rel="noopener noreferrer">Go Pro</a>
		</div>
	</div>
</div>